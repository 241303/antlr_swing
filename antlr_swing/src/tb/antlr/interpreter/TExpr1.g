tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out % $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = (int)Math.pow($e1.out,$e2.out);}
        | ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
        | BS {local.enterScope();}
        | BE {local.leaveScope();}
        | ^(VAR name=ID)           {local.newSymbol($name.text);}
        | ^(PODST i1=ID   e2=expr) {local.setSymbol($i1.text, $e2.out);}
        | INT {$out = getInt($INT.text);}
        | ID {drukuj((local.getSymbol($ID.text)).toString());}
        ;
